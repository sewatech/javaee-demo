#Exemple d'application JavaEE
 
 Testée avec [WildFly 8.1](http://wildfly.org).
 
 Application Web : 
 
 * http://localhost:8080/javaee-demo
 
 Service REST :
 
 * curl -w "\n" -H "accept:application/json" http://localhost:8080/javaee-demo/rest/courses

 Services SOAP : 
 
 * http://localhost:8080/javaee-demo/Course?wsdl
 