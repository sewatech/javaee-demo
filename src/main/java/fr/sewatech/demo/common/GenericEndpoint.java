package fr.sewatech.demo.common;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

public abstract class GenericEndpoint<E extends GenericEntity, S extends GenericService<E>> {

	@Inject
	private Instance<S> serviceInstance;
	private S service;
	private Class<E> entityClass;

	@SuppressWarnings("unchecked")
	public GenericEndpoint() {	
		entityClass = (Class<E>) GenericsUtil.getTypeArguments(GenericEndpoint.class, this.getClass()).get(0);
	}
	
	@PostConstruct
	public void init() {
		service = serviceInstance.get();
	}

	
	@POST
	@Consumes({ "application/xml", "application/json" })
	public Response create(E entity) {
		service.create(entity); 
		return Response.created(
				UriBuilder.fromResource(entityClass)
						  .path(String.valueOf(entity.getId())).build()).build();
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces({ "application/xml", "application/json" })
	public Response findById(@PathParam("id") final Long id) {
		E entity = service.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(entity).build();
	}

	@GET
	@Produces({ "application/xml", "application/json" })
	public List<E> listAll() {
		List<E> courses = service.findAll();
		return courses;
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes({ "application/xml", "application/json" })
	public Response update(@PathParam("id") Long id, E entity) {
		service.update(entity); 
		return Response.noContent().build();
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteById(@PathParam("id") Long id) {
		E entity = service.findById(id);
		service.remove(entity);
		return Response.noContent().build();
	}
}
