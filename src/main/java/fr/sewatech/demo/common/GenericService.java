package fr.sewatech.demo.common;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class GenericService<E extends GenericEntity> {
	
	@PersistenceContext
	private EntityManager em;

	private Class<E> entityClass;
	
	@SuppressWarnings("unchecked")
	public GenericService() {	
		entityClass = (Class<E>) GenericsUtil.getTypeArguments(GenericService.class, this.getClass()).get(0);
	}
	
	public E findById(Long id) {
		return em.find(entityClass, id);
	}
	
	public List<E> findAll() {
		return em.createQuery("SELECT o FROM " + entityClass.getSimpleName() + " o", entityClass)
                 .getResultList();
	}
	
	public E update(E entity) {
		return em.merge(entity);
	}
	
	public E create(E entity) {
		em.persist(entity);
		return entity;
	}
	
	public void remove(E entity) {
		em.remove(em.merge(entity));
	}
}
