package fr.sewatech.demo.common;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.jws.WebService;

@WebService
public abstract class GenericWebService<E extends GenericEntity, S extends GenericService<E>> {

	@Inject
	private Instance<S> serviceInstance;
	private S service;
	
	@PostConstruct
	public void init() {
		service = serviceInstance.get();
	}
	
	public List<E> findAll() {
	    return service.findAll();
	}

	public E findById(Long id) {
	    return service.findById(id);
	}
		
	public E update(E entity) {
		return service.update(entity);
	}
	
	public E create(E entity) {
		return service.create(entity);
	}
	
	public void remove(E entity) {
		service.remove(entity);
	}

}
