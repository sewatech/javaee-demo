package fr.sewatech.demo.common;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenericsUtil<T> {

	public static <T> List<Type> getTypeArguments(Class<T> genericClass, Class<? extends T> childClass) {
		Map<Type, Type> resolvedTypes = new HashMap<Type, Type>();

		internalGetTypeArguments(genericClass, childClass, resolvedTypes);

		Type[] typeParameters = genericClass.getTypeParameters();
		List<Type> typeArguments = new ArrayList<Type>();

		for (Type baseType : typeParameters) {
			while (resolvedTypes.containsKey(baseType)) {
				baseType = resolvedTypes.get(baseType);
			}
			typeArguments.add(baseType);
		}

		return typeArguments;
	}

	private static void internalGetTypeArguments(Class<?> baseClass, Type type, Map<Type, Type> resolvedTypes) {
		if (type == null || baseClass.isAssignableFrom(getClass(type)) == false) {
			return;
		} else {
			if (type instanceof Class) {
				// process superclass
				Type nextType = ((Class<?>) type).getGenericSuperclass();
				internalGetTypeArguments(baseClass, nextType, resolvedTypes);
				// process interfaces
				Type[] nextTypes = ((Class<?>) type).getGenericInterfaces();
				for (Type nextIType : nextTypes) {
					internalGetTypeArguments(baseClass, nextIType,
							resolvedTypes);
				}
			} else {
				ParameterizedType parameterizedType = (ParameterizedType) type;
				Class<?> rawType = (Class<?>) parameterizedType.getRawType();

				Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
				TypeVariable<?>[] typeParameters = rawType.getTypeParameters();

				for (int i = 0; i < actualTypeArguments.length; i++) {
					resolvedTypes.put(typeParameters[i], actualTypeArguments[i]);
				}

				Type nextType = rawType.getGenericSuperclass();
				internalGetTypeArguments(baseClass, nextType, resolvedTypes);
				Type[] nextTypes = rawType.getGenericInterfaces();
				for (Type nextIType : nextTypes) {
					internalGetTypeArguments(baseClass, nextIType, resolvedTypes);
				}
			}
		}
	}

	private static Class<?> getClass(Type type) {
		if (type instanceof Class) {
			return (Class<?>) type;
		} else if (type instanceof ParameterizedType) {
			return getClass(((ParameterizedType) type).getRawType());
		} else if (type instanceof GenericArrayType) {
			Type componentType = ((GenericArrayType) type)
					.getGenericComponentType();
			Class<?> componentClass = getClass(componentType);
			if (componentClass != null) {
				return Array.newInstance(componentClass, 0).getClass();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}
