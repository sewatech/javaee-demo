package fr.sewatech.demo.common;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Produces;

@Singleton
public class JpaProducer {

	@PersistenceContext(name="primary")
	private EntityManager em;
	
	@Produces
	public EntityManager buildEm() {
		return em;
	}
	
}
