package fr.sewatech.demo.ejb;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import fr.sewatech.demo.common.GenericService;
import fr.sewatech.demo.model.Course;

@Stateless @LocalBean
public class CourseService extends GenericService<Course> {
}
