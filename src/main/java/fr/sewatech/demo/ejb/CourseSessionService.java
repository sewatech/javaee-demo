package fr.sewatech.demo.ejb;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import fr.sewatech.demo.common.GenericService;
import fr.sewatech.demo.model.CourseSession;

@Stateless @LocalBean
public class CourseSessionService extends GenericService<CourseSession> {

	public void validate(CourseSession courseSession) {
		super.update(courseSession);
	}
	
}
