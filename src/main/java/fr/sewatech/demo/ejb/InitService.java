package fr.sewatech.demo.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import fr.sewatech.demo.model.Course;

@Singleton @Startup
public class InitService {
	
	@Inject private CourseService courseService;
	
	@PostConstruct
	public void init() {
		createCourse("111", "Course number 1");
		createCourse("222", "Course number 2");
		createCourse("333", "Course number 3");
	}

	private Course createCourse(String code, String description) {
		Course course = new Course();
		course.setCode(code);
		course.setTitle(description);
		course.setSyllabus("Automatically created");
		courseService.create(course);

		return course;
	}
}
