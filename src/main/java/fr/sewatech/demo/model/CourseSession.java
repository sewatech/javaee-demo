package fr.sewatech.demo.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import fr.sewatech.demo.common.GenericEntity;

@Entity @XmlRootElement
public class CourseSession extends GenericEntity {

	public CourseSession buildFromCourse(Course course) {
		CourseSession courseSession = new CourseSession();
		courseSession.setTemplate(course);
		courseSession.setSyllabus(course.getSyllabus());
		return courseSession;
	}
	
	private String code;
	private String title;
	private String syllabus;
	
	@ManyToOne
	private Course template;
	
	@Temporal(TemporalType.DATE)
	private Date start;
	@Temporal(TemporalType.DATE)
	private Date end;
	
	@ManyToOne
	private Teacher teacher;
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public String getSyllabus() {
		return syllabus;
	}

	public void setSyllabus(String syllabus) {
		this.syllabus = syllabus;
	}

	public Course getTemplate() {
		return template;
	}

	public void setTemplate(Course template) {
		this.template = template;
	}
	
	
}
