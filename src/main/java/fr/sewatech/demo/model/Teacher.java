package fr.sewatech.demo.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import fr.sewatech.demo.common.GenericEntity;

@Entity
public class Teacher extends GenericEntity {

	private String code;
	private String firstname;
	private String lastname;
	
	@ManyToMany
	private Set<Course> courses;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Teacher other = (Teacher) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Teacher [code=" + code + ", firstname=" + firstname
				+ ", lastname=" + lastname + "]";
	}
	
	
	
}
