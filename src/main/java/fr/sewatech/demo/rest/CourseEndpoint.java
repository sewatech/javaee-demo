package fr.sewatech.demo.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.Providers;

import fr.sewatech.demo.ejb.CourseService;
import fr.sewatech.demo.model.Course;

@RequestScoped
@Path("/courses")
@Consumes({ MediaType.APPLICATION_XML, "application/json" })
@Produces({ MediaType.APPLICATION_XML, "application/json" })
public class CourseEndpoint {

	@Inject
	private CourseService service;
	
    @Context
    protected Providers providers;

	@POST
	public Response create(Course course) {
		service.create(course); 
		return Response.created(
				UriBuilder.fromResource(Course.class)
						  .path(String.valueOf(course.getId()))
						  .build())
					   .build();
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	public Response findById(@PathParam("id") final Long id) { 
		Course course = service.findById(id);
		if (course == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(course).build();
	}

	@GET
	@Path("/all")
	public Response listAll() {
		List<Course> courses = service.findAll();
		return Response.ok(new GenericEntity<List<Course>>(courses) {}).build();
	}
	@GET
	public List<Course> listAll0() {
		List<Course> courses = service.findAll();
		return courses;
	}
	
	@PUT
	@Path("/{id:[0-9][0-9]*}")
	public Response update(@PathParam("id") Long id, final Course course) {
		service.update(course);
		return Response.noContent().build();
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteById(@PathParam("id") final Long id) {
		service.remove(service.findById(id)); 
		return Response.noContent().build();
	}

}
