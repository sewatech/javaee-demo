package fr.sewatech.demo.rest;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Path;

import fr.sewatech.demo.common.GenericEndpoint;
import fr.sewatech.demo.ejb.CourseSessionService;
import fr.sewatech.demo.model.CourseSession;

@Path("/session")
@RequestScoped
public class CourseSessionEndpoint extends GenericEndpoint<CourseSession, CourseSessionService> {

}
