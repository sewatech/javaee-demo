package fr.sewatech.demo.web;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import fr.sewatech.demo.ejb.CourseService;
import fr.sewatech.demo.model.Course;

@Named 
@ConversationScoped
public class CourseBean implements Serializable {
	
	@Inject
	private CourseService courseService;
	private Course current = new Course();
	
	public List<Course> all() {
		return courseService.findAll();
	}
	
	public void save() {
		if (current.getId() == null) {
			current = courseService.create(current);
		} else {
			current = courseService.update(current);
		}
	}
	public void remove() {
		if (current.getId() != null) {
			courseService.remove(current);
		}
		current = new Course();
	}
	public void clear() {
		current = new Course();
	}

	public Course getCurrent() {
		return current;
	}

	public void setCurrent(Course current) {
		this.current = current;
	}
	
	public void select(Course course) {
		current = course;
	}
	
}
