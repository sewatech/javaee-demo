package fr.sewatech.demo.web;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;

import fr.sewatech.demo.ejb.CourseSessionService;
import fr.sewatech.demo.model.CourseSession;

@Named 
@ConversationScoped
public class CourseSessionBean extends GenericBean<CourseSession, CourseSessionService> {
	
}
