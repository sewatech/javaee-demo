package fr.sewatech.demo.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import fr.sewatech.demo.common.GenericService;
import fr.sewatech.demo.common.GenericEntity;
import fr.sewatech.demo.common.GenericsUtil;

public abstract class GenericBean<E extends GenericEntity, S extends GenericService<E>> implements Serializable {
	
	@Inject
	private Instance<S> serviceInstance;
	private S service;
	private E current;
	private Class<E> entityClass;
	
	@SuppressWarnings("unchecked")
	public GenericBean() {	
		entityClass = (Class<E>) GenericsUtil.getTypeArguments(GenericBean.class, this.getClass()).get(0);
	}
	
	@PostConstruct
	public void init() {
		service = serviceInstance.get();
		current = newEntity();
	}

	public List<E> all() {
		return service.findAll();
	}
	
	public void save() {
		if (current.getId() == null) {
			current = service.create(current);
		} else {
			current = service.update(current);
		}
	}
	public void remove() {
		if (current.getId() != null) {
			service.remove(current);
		}
		current = newEntity();
	}

	public void clear() {
		current = newEntity();
	}

	public E getCurrent() {
		return current;
	}

	public void setCurrent(E current) {
		this.current = current;
	}
	
	public void select(E course) {
		current = course;
	}
	private E newEntity() {
		try {
			return entityClass.newInstance();
		} catch (Exception e) {
			return null; // pas beau
		}
	}

	
}
