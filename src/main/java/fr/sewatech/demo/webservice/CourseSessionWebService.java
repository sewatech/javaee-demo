package fr.sewatech.demo.webservice;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;

import fr.sewatech.demo.common.GenericWebService;
import fr.sewatech.demo.ejb.CourseSessionService;
import fr.sewatech.demo.model.CourseSession;

@WebService(name="CourseSession")
@Stateless @LocalBean
public class CourseSessionWebService extends GenericWebService<CourseSession, CourseSessionService>{

}
