package fr.sewatech.demo.webservice;

import java.util.List;

import javax.inject.Inject;
import javax.jws.WebService;

import fr.sewatech.demo.ejb.CourseService;
import fr.sewatech.demo.model.Course;

@WebService
public class CourseWebService {

	@Inject
	private CourseService service;
	
	public List<Course> findAll() {
	    return service.findAll();
	}

	public Course findById(Long id) {
	    return service.findById(id);
	}
}
